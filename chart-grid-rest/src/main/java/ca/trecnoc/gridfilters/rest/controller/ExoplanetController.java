package ca.trecnoc.gridfilters.rest.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ca.trecnoc.gridfilters.jpa.Exoplanet;
import ca.trecnoc.gridfilters.jpa.filter.FilterSpecificationHelper;
import ca.trecnoc.gridfilters.jpa.repository.ExoplanetRepository;
import ca.trecnoc.gridfilters.jpa.stats.StatsController;

@RestController
@RequestMapping("/exoplanet")
public class ExoplanetController extends StatsController<Exoplanet> {

    @Autowired
    private ExoplanetRepository repository;

    @Autowired
    private FilterSpecificationHelper filterSpecificationHelper;

    ExoplanetController(@Autowired ExoplanetRepository repository) {
        super(Exoplanet.class, repository);
        this.repository = repository;
    }

    @CrossOrigin
    @GetMapping("{id}")
    public Optional<Exoplanet> getExoplanet(@PathVariable("id") Integer id) {
        return repository.findById(id);
    }

    @CrossOrigin
    @GetMapping("")
    public List<Exoplanet> getExoplanets(@RequestParam() final Map<String, String> params) {
        return repository
            .findAll(filterSpecificationHelper.buildFilterSpecifications(params, Exoplanet.class));
    }

    @CrossOrigin
    @GetMapping("discoveryMethods")
    public List<String> getDiscoveryMethods() {
        return repository.findDistinctDiscoveryMethods();
    }

    @CrossOrigin
    @GetMapping("facilities")
    public List<String> getFacilities() {
        return repository.findDistinctFacilities();
    }

}
