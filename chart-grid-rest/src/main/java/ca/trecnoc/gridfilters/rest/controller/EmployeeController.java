package ca.trecnoc.gridfilters.rest.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ca.trecnoc.gridfilters.jpa.Employee;
import ca.trecnoc.gridfilters.jpa.filter.FilterFieldRepository;
import ca.trecnoc.gridfilters.jpa.repository.EmployeeRepository;
import ca.trecnoc.gridfilters.jpa.spec.FilterSpecificationFactory;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private FilterSpecificationFactory specFactory;

    @Autowired
    private FilterFieldRepository filterFields;

    @CrossOrigin
    @GetMapping("/employees/{id}")
    public Optional<Employee> getEmployee(@PathVariable("id") Integer id) {
        return employeeRepository.findById(id);
    }

    @CrossOrigin
    @GetMapping("/employees")
    public List<Employee> getEmployees(@RequestParam() Map<String, String> params) {

        List<Specification<Employee>> specs = filterFields.getFilterDefinitions(Employee.class).stream()
                .filter(def -> params.keySet().contains(def.getFilter()))
                .map(def -> specFactory.build(def.getAttribute(), params.get(def.getFilter()), Employee.class))
                .collect(Collectors.toList());

        return employeeRepository.findAll(specs.stream().reduce(Specification.where((Specification<Employee>) null),
                (Specification<Employee> result, Specification<Employee> current) -> result.and(current)));
    }

}
