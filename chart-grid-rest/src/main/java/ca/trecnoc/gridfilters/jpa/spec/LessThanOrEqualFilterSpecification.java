package ca.trecnoc.gridfilters.jpa.spec;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class LessThanOrEqualFilterSpecification<E> extends FilterSpecification<E> {

    private static final long serialVersionUID = -3992970855342835584L;

    private static final String BAD_TYPE_MESSAGE = "Value for GreaterThanPredicate must be of type Number or Date but was %s";

    @Override
    public Predicate toPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        if (filter.getValue() instanceof Number) {
            return criteriaBuilder.le((Expression<Number>) getFieldExpression(root), (Number) filter.getValue());
        } else if (filter.getValue() instanceof Date) {
            return criteriaBuilder.lessThanOrEqualTo((Expression<Date>) getFieldExpression(root), (Date) filter.getValue());
        } else {
            throw new IllegalArgumentException(String.format(BAD_TYPE_MESSAGE, filter.getValue().getClass().getName()));
        }
    }

}
