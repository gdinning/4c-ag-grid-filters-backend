package ca.trecnoc.gridfilters.jpa.stats;

import java.util.List;
import java.util.function.BiFunction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public interface StatsRepository<E> {

        public Stat getStats(Class<E> clazz, String aggOperation, String aggColumn, Specification<E> filters);

        public Stat getStats(Class<E> clazz,
                String aggOperation,
                BiFunction<CriteriaBuilder, Root<E>, Expression<E>> aggFunction,
                Specification<E> filters);

        public List<Stat> getStats(Class<E> clazz,
                String aggOperation,
                String aggColumn,
                String groupColumn,
                Specification<E> filters);

        public List<Stat> getStats(Class<E> clazz,
                String aggOperation,
                BiFunction<CriteriaBuilder, Root<E>, Expression<E>> aggFunction,
                BiFunction<CriteriaBuilder, Root<E>, Expression<E>> groupFunction,
                Specification<E> filters);

        public List<NamedSeries> getStats(Class<E> clazz,
                String aggOperation,
                String aggColumn,
                String groupColumn,
                String secondaryGroupColumn,
                Specification<E> filters);

        public List<NamedSeries> getStats(Class<E> clazz,
                String aggOperation,
                BiFunction<CriteriaBuilder, Root<E>, Expression<E>> aggFunction,
                BiFunction<CriteriaBuilder, Root<E>, Expression<E>> groupFunction,
                BiFunction<CriteriaBuilder, Root<E>, Expression<E>> secondaryGroupFunction,
                Specification<E> filters);

}
