package ca.trecnoc.gridfilters.jpa.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class NotContainsFilterSpecification<E> extends FilterSpecification<E> {

    private static final long serialVersionUID = -4944546720051538414L;
    private static final String FILTER_TYPE_ERROR_MESSAGE = "ContainsFilterSpecification requires a filter value of type String, but type was %s";

    @Override
    public Predicate toPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        if (!(filter.getValue() instanceof String)) {
            throw new IllegalArgumentException(
                    String.format(FILTER_TYPE_ERROR_MESSAGE, filter.getValue().getClass().getName()));
        }
        return criteriaBuilder.notLike(criteriaBuilder.lower((Expression<String>) getFieldExpression(root)),
                String.format("%%%s%%", ((String) filter.getValue()).toLowerCase()));
    }

}
