package ca.trecnoc.gridfilters.jpa.spec;

import java.util.Base64.Decoder;
import java.util.function.Function;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ca.trecnoc.gridfilters.jpa.filter.TextFilter;
import lombok.Setter;

@Component
@Setter
public class FilterSpecificationFactory {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private Decoder base64Decoder;

    public <E> FilterSpecification<E> build(String field, String value, Class<E> entityClass) {
        return build((Root<E> root) -> root.get(field), value, entityClass);
    }

    public <E> FilterSpecification<E> build(Function<Root<E>, Path<?>> fieldGetter, String value,
            Class<E> entityClass) {
        try {
            String decoded = new String(base64Decoder.decode(value));
            FilterSpecification<E> filterSpec = objectMapper.readValue(decoded,
                    new TypeReference<FilterSpecification<E>>() {
                    });
            filterSpec.setFieldGetter(fieldGetter);
            return filterSpec;
        } catch (IllegalArgumentException | JsonProcessingException e) {
            return getDefaultFilterSpecification(fieldGetter, value, entityClass);
        }
    }

    private <E> FilterSpecification<E> getDefaultFilterSpecification(Function<Root<E>, Path<?>> fieldGetter,
            String value, Class<E> entityClass) {
        TextFilter filter = new TextFilter();
        filter.setValue(value);
        FilterSpecification<E> filterSpec = new EqualsFilterSpecification<E>();
        filterSpec.setFieldGetter(fieldGetter);
        filterSpec.setFilter(filter);
        return filterSpec;
    }

}
