package ca.trecnoc.gridfilters.jpa.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import ca.trecnoc.gridfilters.jpa.filter.SetFilter;

public class SetFilterSpecification<E> extends FilterSpecification<E> {

    private static final long serialVersionUID = 8072071912871622823L;

    private static final String FILTER_TYPE_ERROR_MESSAGE = "SetFilterSpecification requires an array filter, but type was %s";

    @Override
    public Predicate toPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        if (!(filter instanceof SetFilter)) {
            throw new IllegalArgumentException(
                    String.format(FILTER_TYPE_ERROR_MESSAGE, filter.getValue().getClass().getName()));
        }
        In<String> inClause = criteriaBuilder.in((Expression<String>) getFieldExpression(root));
        for (String value : ((SetFilter) filter).getValue()) {
            inClause.value(value);
        }
        return inClause;
    }

}
