package ca.trecnoc.gridfilters.jpa.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class NotEqualsFilterSpecification<E> extends FilterSpecification<E> {

    private static final long serialVersionUID = -3992970855342835584L;

    @Override
    public Predicate toPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.notEqual(getFieldExpression(root), filter.getValue());
    }

}
