package ca.trecnoc.gridfilters.jpa.stats;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NamedSeries {

    private String name;
    private List<Stat> series;

}
