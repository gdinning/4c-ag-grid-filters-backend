package ca.trecnoc.gridfilters.jpa;

import javax.persistence.Entity;
import javax.persistence.Id;

import ca.trecnoc.gridfilters.jpa.filter.Filtered;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class Employee {

    @Id
    private Integer id;

    @Filtered("employeeName")
    private String name;

    @Filtered
    private Integer sickDays;

}
