package ca.trecnoc.gridfilters.jpa.spec;

import java.util.function.Function;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import org.springframework.data.jpa.domain.Specification;

import ca.trecnoc.gridfilters.jpa.filter.Filter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@JsonTypeInfo(use = Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "predicate")
@JsonSubTypes({ @Type(value = ContainsFilterSpecification.class, name = "contains"),
        @Type(value = NotContainsFilterSpecification.class, name = "notContains"),
        @Type(value = EqualsFilterSpecification.class, name = "equals"),
        @Type(value = NotEqualsFilterSpecification.class, name = "notEqual"),
        @Type(value = GreaterThanFilterSpecification.class, name = "greaterThan"),
        @Type(value = GreaterThanOrEqualFilterSpecification.class, name = "greaterThanOrEqual"),
        @Type(value = LessThanFilterSpecification.class, name = "lessThan"),
        @Type(value = LessThanOrEqualFilterSpecification.class, name = "lessThanOrEqual"),
        @Type(value = InRangeFilterSpecification.class, name = "inRange"),
        @Type(value = SetFilterSpecification.class, name = "set"),
        @Type(value = AndFilterSpecification.class, name = "AND"),
        @Type(value = OrFilterSpecification.class, name = "OR") })
public abstract class FilterSpecification<E> implements Specification<E> {

    private static final long serialVersionUID = 2716002690246402576L;

    private Function<Root<E>, Path<?>> fieldGetter;

    protected Filter<?> filter;

    protected Path<?> getFieldExpression(Root<E> root) {
        return fieldGetter.apply(root);
    }

}