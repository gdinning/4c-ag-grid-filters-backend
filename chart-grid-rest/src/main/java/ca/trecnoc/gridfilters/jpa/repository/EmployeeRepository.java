package ca.trecnoc.gridfilters.jpa.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import ca.trecnoc.gridfilters.jpa.Employee;

public interface EmployeeRepository
        extends PagingAndSortingRepository<Employee, Integer>, JpaSpecificationExecutor<Employee> {

}
