package ca.trecnoc.gridfilters.jpa;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import ca.trecnoc.gridfilters.jpa.filter.Filtered;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class Exoplanet {

    @Id
    @Filtered("exoplanetId")
    private Integer id;

    private String planetLetter;

    @Filtered
    private String name;

    @Filtered
    private String discoveryMethod;

    @Filtered
    private Date lastUpdated;

    @Filtered
    private String facility;

    @Filtered
    private String telescope;

    @Filtered
    private String instrument;

    private String link;

    @ManyToOne
    @JoinColumn(
        name = "star_id")
    private Star star;

}
