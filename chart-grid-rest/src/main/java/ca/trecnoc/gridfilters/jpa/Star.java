package ca.trecnoc.gridfilters.jpa;

import javax.persistence.Entity;
import javax.persistence.Id;

import ca.trecnoc.gridfilters.jpa.filter.Filtered;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Star {

    @Id
    private Integer id;

    @Filtered("starName")
    private String name;

    @Filtered
    private Integer planetCount;

}
