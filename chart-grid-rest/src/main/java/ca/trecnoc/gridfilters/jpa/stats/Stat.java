package ca.trecnoc.gridfilters.jpa.stats;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Stat {
    private String name;
    private Object value;
}