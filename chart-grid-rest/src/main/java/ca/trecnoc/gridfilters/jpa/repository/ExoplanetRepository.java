package ca.trecnoc.gridfilters.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ca.trecnoc.gridfilters.jpa.Exoplanet;
import ca.trecnoc.gridfilters.jpa.stats.StatsRepository;

public interface ExoplanetRepository
    extends CrudRepository<Exoplanet, Integer>, JpaSpecificationExecutor<Exoplanet>, StatsRepository<Exoplanet> {

    @Query("SELECT DISTINCT discoveryMethod FROM Exoplanet")
    List<String> findDistinctDiscoveryMethods();

    @Query("SELECT DISTINCT facility FROM Exoplanet")
    List<String> findDistinctFacilities();

}
