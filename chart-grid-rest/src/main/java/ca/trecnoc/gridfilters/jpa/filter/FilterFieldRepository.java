package ca.trecnoc.gridfilters.jpa.filter;

import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.EntityType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FilterFieldRepository {

    @Autowired
    private EntityManager em;

    private Map<String, FilterFieldDefinition> definitions = new HashMap<>();

    @PostConstruct
    public void init() {
        em.getMetamodel().getEntities().forEach((EntityType<?> entity) -> {
            entity.getAttributes().forEach((Attribute<?, ?> attr) -> {
                Member member = attr.getJavaMember();
                if (member instanceof Field) {
                    Field field = (Field) member;
                    Class<?> clazz = field.getDeclaringClass();
                    String name = field.getName();
                    Filtered annotation = field.getAnnotation(Filtered.class);
                    if (annotation != null) {
                        String filterName = "".equals(annotation.value()) ? name : annotation.value();
                        this.definitions.put(filterName, new FilterFieldDefinition(filterName, clazz, name));
                    }
                }
            });
        });
    }

    public FilterFieldDefinition getFilterDefinition(String filterName) {
        return this.definitions.get(filterName);
    }

    public boolean hasFilterDefinition(String filterName) {
        return this.definitions.keySet().contains(filterName);
    }

    public Set<FilterFieldDefinition> getFilterDefinitions(Class<?> clazz) {
        return this.definitions.values().stream().filter(def -> def.getClazz().equals(clazz))
                .collect(Collectors.toSet());
    }

}