package ca.trecnoc.gridfilters.jpa.filter;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import org.springframework.lang.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@JsonTypeInfo(use = Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = TextFilter.class, name = "text"), @Type(value = NumberFilter.class, name = "number"),
        @Type(value = DateFilter.class, name = "date"), @Type(value = SetFilter.class, name = "set") })
public abstract class Filter<T> {

    private T value;

    @Nullable
    private T toValue;
}
