package ca.trecnoc.gridfilters.jpa.stats;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.persistence.Tuple;

public class NamedSeriesCollector implements Collector<Tuple, Map<String, List<Stat>>, List<NamedSeries>> {

    @Override
    public Supplier<Map<String, List<Stat>>> supplier() {
        return HashMap<String, List<Stat>>::new;
    }

    @Override
    public BiConsumer<Map<String, List<Stat>>, Tuple> accumulator() {
        return (Map<String, List<Stat>> map, Tuple tuple) -> {
            String name = tuple.get("name").toString();
            if (!map.keySet().contains(name)) {
                map.put(name, new ArrayList<Stat>());
            }
            map.get(name).add(new Stat(tuple.get("series").toString(), tuple.get("value")));
        };
    }

    @Override
    public BinaryOperator<Map<String, List<Stat>>> combiner() {
        return (Map<String, List<Stat>> a, Map<String, List<Stat>> b) -> {
            Map<String, List<Stat>> result = new HashMap<>(a);
            b.keySet().forEach(key -> result.put(key, b.get(key)));
            return result;
        };
    }

    @Override
    public Function<Map<String, List<Stat>>, List<NamedSeries>> finisher() {
        return (Map<String, List<Stat>> map) -> map.entrySet().stream()
            .map(entry -> new NamedSeries(entry.getKey(), entry.getValue())).collect(Collectors.toList());
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Collections.unmodifiableSet(new HashSet<>(Arrays.asList(Characteristics.UNORDERED)));
    }

}
