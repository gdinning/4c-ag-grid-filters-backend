package ca.trecnoc.gridfilters.jpa.spec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class OrFilterSpecification<E> extends BooleanFilterSpecification<E> {

    private static final long serialVersionUID = 6503926991240504789L;

    @Override
    public Predicate toPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.or(condition1.toPredicate(root, query, criteriaBuilder),
                condition2.toPredicate(root, query, criteriaBuilder));
    }

}
