package ca.trecnoc.gridfilters.jpa.stats;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import ca.trecnoc.gridfilters.jpa.filter.FilterFieldDefinition;
import ca.trecnoc.gridfilters.jpa.filter.FilterFieldRepository;
import ca.trecnoc.gridfilters.jpa.filter.FilterSpecificationHelper;

public abstract class StatsController<E> {

    @Autowired
    private FilterSpecificationHelper filterSpecificationHelper;

    @Autowired
    private FilterFieldRepository filterFields;

    private Class<E> clazz;
    private StatsRepository<E> repository;

    protected StatsController(Class<E> clazz, StatsRepository<E> repository) {
        this.clazz = clazz;
        this.repository = repository;
    }

    @CrossOrigin
    @GetMapping("stats/{operation}/{aggregate}")
    public Stat getStats(@RequestParam() final Map<String, String> params,
        @PathVariable("operation") String operation,
        @PathVariable("aggregate") String aggregate) {

        return repository.getStats(
            clazz,
            operation,
            getExpression(aggregate),
            filterSpecificationHelper.buildFilterSpecifications(params, clazz));
    }

    @CrossOrigin
    @GetMapping("stats/{group}/{operation}/{aggregate}")
    public List<Stat> getStats(@RequestParam() final Map<String, String> params,
        @PathVariable("group") String group,
        @PathVariable("operation") String operation,
        @PathVariable("aggregate") String aggregate) {
        return repository.getStats(clazz,
            operation,
            getExpression(aggregate),
            getExpression(group),
            filterSpecificationHelper.buildFilterSpecifications(params, clazz));
    }

    @CrossOrigin
    @GetMapping("stats/{group}/{group2}/{operation}/{aggregate}")
    public List<NamedSeries> getStats(@RequestParam() final Map<String, String> params,
        @PathVariable("group") String group,
        @PathVariable("group2") String group2,
        @PathVariable("operation") String operation,
        @PathVariable("aggregate") String aggregate) {

        return repository.getStats(
            clazz,
            operation,
            getExpression(aggregate),
            getExpression(group),
            getExpression(group2),
            filterSpecificationHelper.buildFilterSpecifications(params, clazz));
    }

    private BiFunction<CriteriaBuilder, Root<E>, Expression<E>> getExpression(String attrName) {
        if (attrName.contains(":")) {
            String[] split = attrName.split(":");
            return getDateExpression(split[0], split[1]);
        }

        FilterFieldDefinition def = filterFields.getFilterDefinition(attrName);
        if (def.getClazz() == clazz) {
            return (CriteriaBuilder cb, Root<E> root) -> root.get(def.getAttribute());
        } else {
            return (CriteriaBuilder cb, Root<E> root) -> root.join(def.getClazz().getSimpleName().toLowerCase())
                .get(def.getAttribute());
        }
    }

    private BiFunction<CriteriaBuilder, Root<E>, Expression<E>> getDateExpression(String attrName, String func) {
        FilterFieldDefinition def = filterFields.getFilterDefinition(attrName);
        if (def.getClazz() == clazz) {
            return (CriteriaBuilder cb, Root<E> root) -> cb.function(func, clazz, root.get(def.getAttribute()));
        } else {
            return (CriteriaBuilder cb, Root<E> root) -> cb.function(func, clazz,
                root.join(def.getClazz().getSimpleName().toLowerCase())
                    .get(def.getAttribute()));
        }
    }

}
