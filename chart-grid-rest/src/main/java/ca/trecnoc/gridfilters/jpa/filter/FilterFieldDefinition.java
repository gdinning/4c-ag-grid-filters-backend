package ca.trecnoc.gridfilters.jpa.filter;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class FilterFieldDefinition {

    private String filter;

    private Class<?> clazz;

    private String attribute;

}
