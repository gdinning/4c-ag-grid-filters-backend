package ca.trecnoc.gridfilters.jpa.stats;

import static java.util.Optional.ofNullable;

import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class StatsRepositoryImpl<E> implements StatsRepository<E> {

    private static final String INVALID_OP_FOR_TYPE_MESSAGE = "Column type %s is not valid for operation %s";

    @PersistenceContext
    private EntityManager entityManager;

    public Stat getStats(Class<E> clazz, String aggOperation, String aggColumn, Specification<E> filters) {
        return getStats(clazz, aggOperation, (CriteriaBuilder cb, Root<E> root) -> root.get(aggColumn), filters);
    }

    public Stat getStats(Class<E> clazz,
        String aggOperation,
        BiFunction<CriteriaBuilder, Root<E>, Expression<E>> aggFunction,
        Specification<E> filters) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> query = cb.createQuery(Tuple.class);
        Root<E> root = query.from(clazz);

        Expression<E> aggExpr = aggFunction.apply(cb, root);

        query.select(cb.tuple(buildAggregate(cb, aggExpr, aggOperation).alias("value")));

        Predicate predicate = filters.toPredicate(root, query, cb);
        if (predicate != null) {
            query.where(predicate);
        }

        return ofNullable(entityManager.createQuery(query).getSingleResult())
            .map(tuple -> new Stat(aggOperation, tuple.get("value"))).orElse(null);
    }

    public List<Stat> getStats(Class<E> clazz,
        String aggOperation,
        String aggColumn,
        String groupColumn,
        Specification<E> filters) {
        return getStats(clazz,
            aggOperation,
            (CriteriaBuilder cb, Root<E> root) -> root.get(aggColumn),
            (CriteriaBuilder cb, Root<E> root) -> root.get(groupColumn),
            filters);
    }

    public List<Stat> getStats(Class<E> clazz,
        String aggOperation,
        BiFunction<CriteriaBuilder, Root<E>, Expression<E>> aggFunction,
        BiFunction<CriteriaBuilder, Root<E>, Expression<E>> groupFunction,
        Specification<E> filters) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> query = cb.createQuery(Tuple.class);
        Root<E> root = query.from(clazz);

        Expression<E> aggExpr = aggFunction.apply(cb, root);
        Expression<E> groupExpr = groupFunction.apply(cb, root);

        query.select(cb.tuple(buildAggregate(cb, aggExpr, aggOperation).alias("value"), groupExpr.alias("name")))
            .groupBy(groupExpr).orderBy(cb.asc(groupExpr));

        Predicate predicate = filters.toPredicate(root, query, cb);
        if (predicate != null) {
            query.where(predicate);
        }

        return entityManager.createQuery(query).getResultList().stream().map((Tuple tuple) -> {
            return new Stat(tuple.get("name").toString(), tuple.get("value"));
        }).collect(Collectors.toList());
    }

    @Override
    public List<NamedSeries> getStats(Class<E> clazz,
        String aggOperation,
        String aggColumn,
        String groupColumn,
        String secondaryGroupColumn,
        Specification<E> filters) {
        return getStats(clazz,
            aggOperation,
            (CriteriaBuilder cb, Root<E> root) -> root.get(aggColumn),
            (CriteriaBuilder cb, Root<E> root) -> root.get(groupColumn),
            (CriteriaBuilder cb, Root<E> root) -> root.get(secondaryGroupColumn),
            filters);
    }

    @Override
    public List<NamedSeries> getStats(Class<E> clazz,
        String aggOperation,
        BiFunction<CriteriaBuilder, Root<E>, Expression<E>> aggFunction,
        BiFunction<CriteriaBuilder, Root<E>, Expression<E>> groupFunction,
        BiFunction<CriteriaBuilder, Root<E>, Expression<E>> secondaryGroupFunction,
        Specification<E> filters) {

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> query = cb.createQuery(Tuple.class);
        Root<E> root = query.from(clazz);

        Expression<E> aggExpr = aggFunction.apply(cb, root);
        Expression<E> groupExpr = groupFunction.apply(cb, root);
        Expression<E> secondaryGroupExpr = secondaryGroupFunction.apply(cb, root);

        query.select(cb.tuple(buildAggregate(cb, aggExpr, aggOperation).alias("value"),
            groupExpr.alias("name"),
            secondaryGroupExpr.alias("series"))).groupBy(groupExpr, secondaryGroupExpr)
            .orderBy(cb.asc(groupExpr), cb.asc(secondaryGroupExpr));

        Predicate predicate = filters.toPredicate(root, query, cb);
        if (predicate != null) {
            query.where(predicate);
        }

        return entityManager.createQuery(query).getResultList().stream().collect(new NamedSeriesCollector());
    }

    private Expression<?> buildAggregate(CriteriaBuilder cb, Expression<E> expr, String operation) {
        switch (operation) {
            case "count":
                return cb.count(expr);
            case "avg":
                if (isNumberExpression(expr)) {
                    return cb.avg((Expression<Number>)expr);
                } else {
                    throwInvalidTypeForOperationException(expr.getJavaType(), operation);
                }
            case "min":
                if (isNumberExpression(expr)) {
                    return cb.min((Expression<Number>)expr);
                } else if (isComparableExpression(expr)) {
                    return cb.least((Expression<Comparable>)expr);
                } else {
                    throwInvalidTypeForOperationException(expr.getJavaType(), operation);
                }
            case "max":
                if (isNumberExpression(expr)) {
                    return cb.max((Expression<Number>)expr);
                } else if (isComparableExpression(expr)) {
                    return cb.greatest((Expression<Comparable>)expr);
                } else {
                    throwInvalidTypeForOperationException(expr.getJavaType(), operation);
                }
            case "sum":
                if (isNumberExpression(expr)) {
                    return cb.sum((Expression<Number>)expr);
                } else {
                    throwInvalidTypeForOperationException(expr.getJavaType(), operation);
                }
            default:
                return expr;
        }
    }

    private boolean isNumberExpression(Expression<E> expr) {
        return Number.class.isAssignableFrom(expr.getJavaType());
    }

    private boolean isComparableExpression(Expression<E> expr) {
        return Comparable.class.isAssignableFrom(expr.getJavaType());
    }

    private void throwInvalidTypeForOperationException(Class<?> type, String operation) {
        throw new IllegalArgumentException(String.format(INVALID_OP_FOR_TYPE_MESSAGE, type, operation));
    }

}
