package ca.trecnoc.gridfilters.jpa.spec;

import java.util.function.Function;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import ca.trecnoc.gridfilters.jpa.filter.Filter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public abstract class BooleanFilterSpecification<E> extends FilterSpecification<E> {

    private static final long serialVersionUID = -8960598543165444422L;

    protected FilterSpecification<E> condition1;
    protected FilterSpecification<E> condition2;

    @Override
    public void setFieldGetter(Function<Root<E>, Path<?>> fieldGetter) {
        condition1.setFieldGetter(fieldGetter);
        condition2.setFieldGetter(fieldGetter);
    }

    @Override
    public void setFilter(Filter filter) {
        condition1.setFilter(filter);
        condition2.setFilter(filter);
    }

}
