package ca.trecnoc.gridfilters.jpa.filter;

import java.util.Map;
import java.util.Objects;

import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import ca.trecnoc.gridfilters.jpa.spec.FilterSpecificationFactory;

@Component
public class FilterSpecificationHelper {

    @Autowired
    private FilterSpecificationFactory filterSpecFactory;

    @Autowired
    private FilterFieldRepository filterFields;

    public <T> Specification<T> buildFilterSpecifications(final Map<String, String> params, Class<T> clazz) {

        return params.keySet().stream()
            .map(key -> filterFields.getFilterDefinition(key))
            .filter(Objects::nonNull)
            .map((FilterFieldDefinition def) -> {
                if (def.getClazz() == clazz) {
                    return (Specification<T>)filterSpecFactory
                        .build(def.getAttribute(), params.get(def.getFilter()), clazz);
                } else {
                    return (Specification<T>)filterSpecFactory.build(
                        (Root<T> root) -> root.join(def.getClazz().getSimpleName().toLowerCase())
                            .get(def.getAttribute()),
                        params.get(def.getFilter()),
                        clazz);
                }
            })
            .reduce(
                Specification.where(null),
                (Specification<T> result,
                    Specification<T> current) -> (Specification<T>)result.and(current));
    }

}
